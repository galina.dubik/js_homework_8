const input = document.getElementById('text')
const span = document.createElement('span')
const button = document.createElement('button')
const buttonClosed = document.createElement('button')
const price = document.createElement('div')
const classPrice = document.querySelector('.price')
const error = document.createElement('span')                           




input.addEventListener('focus', function(){
        this.style.border = '3px solid green'
})

input.addEventListener('blur', function(){
    if(input.value < 0){
        this.style.outline = 'none'
        this.style.border = '3px solid red'
        error.textContent = "Please enter correct price"
        error.style.color = 'red'
        error.style.display= 'block'
        classPrice.append(error)
        
    } else if (input.value > 0) {
            span.innerText = `Current price: ${input.value}`
            
            input.style.color = 'green'
            price.prepend(span)
            error.remove() 

            buttonClosed.innerText = 'x'
            price.append(buttonClosed)
        
    }
    // this.style.border = '1px solid black'
    document.body.prepend(price)
    
    console.log(input.value);

})

buttonClosed.addEventListener('click',() =>{
    input.value = '0'
    input.style.color = 'black'
    span.remove()
    buttonClosed.remove()
})